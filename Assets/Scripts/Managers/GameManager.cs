﻿using System.Collections;
using System.Collections.Generic;
using CF.Utils;
using CF.Utils.Events;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{

	public static EventX OnNewSession = new EventX();

	[Header("Game Management")]
	public List<GameManagerWave> configurationWave;

	[Header("Family Generator")]
	public FamilyGenerator familyGeneratorPrefab;
	[Range(0f, .9f)] public float chairOcupationPercent = .85f;
	public float separationBetweenFamilies = .75f;
	public GameManagerState currentGameState = GameManagerState.None;

	[Header("Rules")]
	public int reputationToFinnish = 1000;
	[Range(0f, 1f)] public float chairsMaxBrokePercent = .1f;
	public bool ignoreGameOver;

	public Transform queueStart;
	public Transform exitPosition;

	[Space]
	public GameData gameData;

	[Header("References")]
	public CanvasGroup hudCanvasGroup;
	public CanvasGroup gameoverCanvasGroup;
	public CanvasGroup fadeCanvas;
	public LightManager lightManager;
	public DoorManager doorManager;

	#region Properties

	public bool IsGameOver
	{
		get
		{
			if (ignoreGameOver) return false;

			return gameData.reputation >= reputationToFinnish;
		}
	}

	public bool IsPlaying
	{
		get
		{
			return currentGameState != GameManagerState.None && currentGameState != GameManagerState.GameOver;
		}
	}

	#endregion

	#region Unity Events

	private void Start()
	{
		StartCoroutine(StartGameRoutine());
	}

	private void Update()
	{
		if (currentGameState == GameManagerState.Sit)
		{
			gameData.roundTime += Time.deltaTime;

			if (Family.allFamilies.Count <= 0) SetState(GameManagerState.SeeFilm);
		}

		if (IsPlaying)
		{
			gameData.playingTime += Time.deltaTime;
		}
	}

	private void OnEnable()
	{
		Dirt.OnClean += OnClean;
		Family.OnPlaceFamily += OnPlaceFamily;
		Family.OnDropFamily += OnDropFamily;
		Person.OnPlacePerson += OnPlacePerson;
	}

	private void OnDisable()
	{
		Dirt.OnClean -= OnClean;
		Family.OnPlaceFamily -= OnPlaceFamily;
		Family.OnDropFamily -= OnDropFamily;
		Person.OnPlacePerson -= OnPlacePerson;
	}

	#endregion

	#region [Public API]

	public int GetScore()
	{
		int baseScore = 0;

		Chair.allChairs.ForEach(chair =>
		{
			if (chair.personInside != null) baseScore += chair.personInside.Score();
		});

		int result = baseScore;

		return result;
	}

	public void ToMainMenu()
	{
		gameoverCanvasGroup.interactable = false;
		gameoverCanvasGroup.blocksRaycasts = false;

		Sequence s = DOTween.Sequence();
		s.Append(fadeCanvas.DOAlpha(1f, 1f));
		s.AppendInterval(.5f);
		s.AppendCallback(() => SceneManager.LoadScene("MainMenu"));
	}

	#endregion

	#region Event Handling

	private void OnClean(Dirt dirt)
	{
		gameData.reputation += GameRules.CLEAN_SCORE;
		gameData.dirtCleaned++;
	}

	private void OnPlaceFamily(Family family)
	{
		LogicNextFamily();
	}

	private void OnDropFamily(Family family)
	{
		LogicNextFamily();

		gameData.reputation = Mathf.Max(gameData.reputation - family.personList.Count * GameRules.DROP_PERSON_SCORE, 0);
		gameData.droppedCustomers += family.personList.Count;
	}

	private void OnPlacePerson(Person person)
	{
		if (person.personType == Person.PersonType.Adult) gameData.placedAdults++;
		else gameData.placedChilds++;
	}

	#endregion

	#region Helpers

	private void LogicNextFamily()
	{
		if (Family.allFamilies.Count > 0)
		{
			AdvanceQueue();
		}
	}

	private void AdvanceQueue()
	{
		int index = 0;
		Family.allFamilies.ForEach(family =>
		{
			// family.transform.position -= transform.forward * separationBetweenFamilies;
			Sequence s = DOTween.Sequence();
			s.AppendInterval(index * .025f);
			s.Append(family.transform.DOMove(family.transform.position - transform.forward * separationBetweenFamilies, .15f));

			index++;
		});
	}

	private void PrepareGame()
	{
		gameData.sessionsPlayed++;
		gameData.roundTime = 0;

		// Broke chairs
		if (gameData.sessionsPlayed > 2)
		{
			float brokePercent = chairsMaxBrokePercent * ((float)gameData.reputation / reputationToFinnish);       // broke is increasing with the reputation
			Chair.allChairs.ForEach(chair =>
			{
				chair.broken = Random.Range(0f, 1f) < brokePercent;
			});
		}

		// Enable chairs with the reputation of the cinema
		configurationWave.ForEach(wave =>
		{
			if (gameData.reputation >= wave.reputationToEnable) wave.SetEnabled();
		});

		int maxChairs = (int)(Chair.allChairs.Count * chairOcupationPercent);

		while (PersonWaiting() < maxChairs)
		{
			InstantiateFamily();
		}
	}

	private void InstantiateFamily()
	{
		var familyGenerator = Instantiate(familyGeneratorPrefab, queueStart.position + Vector3.forward * separationBetweenFamilies * Family.allFamilies.Count, Quaternion.identity);
		familyGenerator.GenerateFamily();
	}

	private int PersonWaiting()
	{
		int sum = 0;
		Family.allFamilies.ForEach(x => sum += x.personList.Count);
		return sum;
	}

	private void SetState(GameManagerState state)
	{
		currentGameState = state;

		switch (state)
		{
			case GameManagerState.Preparing: StartPreparingState(); break;
			case GameManagerState.Sit: StartSitState(); break;
			case GameManagerState.SeeFilm: StartSeeFilmState(); break;
			case GameManagerState.Exit: StartExitState(); break;
			case GameManagerState.GameOver: StartGameOver(); break;
			case GameManagerState.None: break;
		}
	}

	private void StartPreparingState()
	{
		StartCoroutine(PreparingRoutine());
	}

	private void StartSitState()
	{
		
	}

	private void StartSeeFilmState()
	{
		StartCoroutine(SeeFilmRoutine());
	}

	private void StartExitState()
	{
		StartCoroutine(ExitRoutine());
	}

	private void StartGameOver()
	{
		CameraManager.Instance.SetCamera(0);

		hudCanvasGroup.DOAlpha(0f, 1f);
		hudCanvasGroup.interactable = false;
		hudCanvasGroup.blocksRaycasts = false;

		gameoverCanvasGroup.DOAlpha(1f, 1f);
		gameoverCanvasGroup.interactable = true;
		gameoverCanvasGroup.blocksRaycasts = true;
	}

	#endregion

	#region Routines

	private IEnumerator StartGameRoutine()
	{
		yield return new WaitUntil(() => Time.deltaTime < .1f);
		yield return new WaitForSeconds(1.5f);

		// ToDo: Camera
		CameraManager.Instance.SetCamera(1);

		SetState(GameManagerState.Preparing);
	}

	private IEnumerator PreparingRoutine()
	{
		if (IsGameOver && !ignoreGameOver)
		{
			SetState(GameManagerState.GameOver);
		}
		else
		{
			yield return new WaitForSeconds(.5f);
			PrepareGame();

			doorManager.SetEnter(true);

			yield return new WaitForSeconds(1f);
			AdvanceQueue();

			SetState(GameManagerState.Sit);
		}
	}

	private IEnumerator SeeFilmRoutine()
	{
		doorManager.SetEnter(false);

		yield return new WaitForSeconds(1f);

		// Film lights
		lightManager.SetFilmLights();
		yield return new WaitForSeconds(2f);

		yield return new WaitForSeconds(1.5f);

		Chair.allChairs.ForEach(chair =>
		{
			if (chair.personInside != null)
			{
				gameData.reputation += chair.personInside.Score();
				chair.personInside.ShowEmote();
			}
		});

		gameData.reputation = Mathf.Max(0, gameData.reputation); // Clamp score to 0

		yield return new WaitForSeconds(1.5f);

		// Cinema lights
		lightManager.SetCinemaLights();
		yield return new WaitForSeconds(1f);

		SetState(GameManagerState.Exit);
	}

	private IEnumerator ExitRoutine()
	{
		int i = 0;

		doorManager.SetExit(true);

		Chair.allChairs.ForEach(chair =>
		{
			if (chair.personInside != null)
			{
				chair.personInside.CheckDirt();

				Sequence sub = DOTween.Sequence();
				sub.AppendInterval(.05f * i);
				sub.Append(chair.personInside.transform.DOMove(exitPosition.position - Vector3.forward * 3f + Vector3.up, .25f));
				sub.Append(chair.personInside.transform.DOMove(exitPosition.position + Vector3.forward * 3f, .45f));
				sub.OnComplete(() =>
				{
					Destroy(chair.personInside.gameObject);
				});

				i++;
			}
		});

		yield return new WaitForSeconds(2.5f);

		doorManager.SetExit(false);

		SetState(GameManagerState.Preparing);
	}

	#endregion

	#region Nested

	[System.Serializable]
	public class GameManagerWave
	{
		public int reputationToEnable;
		public List<GameObject> objectsToEnable;

		public void SetEnabled()
		{
			objectsToEnable.ForEach(x => x.gameObject.SetActive(true));
		}
	}

	public enum GameManagerState
	{
		Preparing, Sit, SeeFilm, Exit, None, GameOver
	}

	#endregion

}
