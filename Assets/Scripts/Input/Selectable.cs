﻿using UnityEngine;

public class Selectable : MonoBehaviour
{

	protected bool selected;

	protected virtual void OnEnable()
	{
		InputManager.OnSelectedObject += OnSelectedObject;
	}

	protected virtual void OnDisable()
	{
		InputManager.OnSelectedObject -= OnSelectedObject;
	}

	protected virtual void OnSelectedObject(GameObject selectedObject)
	{
		if (!selected && selectedObject == gameObject)
		{
			selected = true;
			SetSelected();
		}
		else if (selected && selectedObject != gameObject)
		{
			selected = false;
			QuitSelected();
		}
	}

	protected virtual void SetSelected()
	{
	}

	protected virtual void QuitSelected()
	{
	}
}
