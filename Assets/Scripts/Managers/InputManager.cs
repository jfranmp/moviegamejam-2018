﻿using CF.Utils.Events;
using Sirenix.OdinInspector;
using UnityEngine;

public class InputManager : MonoBehaviour
{

	public static EventX<GameObject> OnChangeHoverObject = new EventX<GameObject>();
	public static EventX<GameObject> OnSelectedObject = new EventX<GameObject>();

	public InputState inputState = InputState.None;
	
	[ReadOnly]
	[System.NonSerialized]
	[ShowInInspector]
	private GameObject hoverObject;

	private Family selectedFamily;

	private new Camera camera;

	#region Unity Events

	private void Awake()
	{
		Init();
	}

	private void Update()
	{
		DoRaycast();
		ProcessInput();
	}

	#endregion

	#region Setup

	private void Init()
	{
		camera = Camera.main;
	}

	#endregion

	#region Helpers

	private void DoRaycast()
	{
		RaycastHit hit;
		if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, 100f))
		{
			SetHoverObject(hit.collider.gameObject);
		}
		else
		{
			SetHoverObject(null);
		}
	}

	private void ProcessInput()
	{
		if (Input.GetMouseButtonDown(0))
		{
			TrySelectFamily();
		}

		if ((Input.GetMouseButtonDown(1) || Input.GetMouseButton(0)) && inputState == InputState.PlaceFamily)
		{
			TryPlaceFamily();
		}

		if (Input.GetKeyDown(KeyCode.Escape) && selectedFamily != null)
		{
			SelectFamily(null);
		}
	}

	private void SetHoverObject(GameObject hoverObject)
	{
		if (this.hoverObject != hoverObject)
		{
			this.hoverObject = hoverObject;
		}

		OnChangeHoverObject.Invoke(hoverObject);
	}

	private void TrySelectFamily()
	{
		if (hoverObject == null)
		{
			SelectFamily(null);
			return;
		}

		var family = hoverObject.GetComponentInParent<Family>();
		if (family != null && Family.allFamilies[0] == family) SelectFamily(family);
		else SelectFamily(null);
	}

	private void SelectFamily(Family family)
	{
		selectedFamily = family;
		inputState = selectedFamily != null ? InputState.PlaceFamily : InputState.None;

		OnSelectedObject.Invoke(selectedFamily == null ? null : selectedFamily.gameObject);
	}

	private void TryPlaceFamily()
	{
		if (hoverObject == null)
		{
			return;
		}

		// Sit
		var chair = hoverObject.GetComponent<Chair>();
		if (chair != null && selectedFamily.CanPlaceFamily(chair))
		{
			selectedFamily.PlaceFamily(chair);
			SelectFamily(null);

			inputState = InputState.None;

			return;
		}

		// Drop
		var dropZone = hoverObject.GetComponent<DropFamily>();
		if (dropZone != null)
		{
			selectedFamily.Drop();
			SelectFamily(null);

			inputState = InputState.None;

			return;
		}
	}

	#endregion

	public enum InputState
	{
		None, PlaceFamily
	}

}