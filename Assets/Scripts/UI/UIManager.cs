﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{

	public TextMeshProUGUI timeText;
	public TextMeshProUGUI reputationText;
	public TextMeshProUGUI sessionText;

	private GameManager gameManager;

	private void Awake()
	{
		gameManager = GameManager.Instance;
	}

	private void Update()
	{
		UpdateText();
	}

	private void UpdateText()
	{
		reputationText.SetText(string.Format("Rep: {0}", gameManager.gameData.reputation));
		sessionText.SetText(string.Format("Session: {0}", gameManager.gameData.sessionsPlayed));
		timeText.SetText(string.Format("Time: {0}:{1:00}", (int)(gameManager.gameData.playingTime / 60), gameManager.gameData.playingTime % 60));
	}

}
