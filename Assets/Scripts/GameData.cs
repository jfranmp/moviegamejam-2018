﻿using Sirenix.OdinInspector;

[System.Serializable]
public class GameData
{

	public int reputation;
	public int sessionsPlayed;
	public int placedAdults;
	public int placedChilds;
	public int droppedCustomers;
	public int dirtCleaned;
	public float roundTime;
	public float playingTime;

	public GameData()
	{
		Reset();
	}

	[Button]
	public void Reset()
	{
		reputation = 0;
		sessionsPlayed = 0;
		placedAdults = 0;
		placedChilds = 0;
		droppedCustomers = 0;
		dirtCleaned = 0;
		roundTime = 0;
		playingTime = 0;
	}
}
