﻿using UnityEngine;

public class Billboard : MonoBehaviour {

	private static Camera _camera;
	public static new Camera camera
	{
		get
		{
			if (_camera == null) _camera = Camera.main;
			return _camera;
		}
	}

	private void Update()
	{
		transform.LookAt(camera.transform.position);
	}

}
