﻿using CF.Utils.Events;
using DG.Tweening;
using UnityEngine;

public class Dirt : MonoBehaviour
{
	public static EventX<Dirt> OnClean = new EventX<Dirt>();

	private void OnEnable()
	{
		Vector3 localScale = transform.localScale;
		transform.localScale = Vector3.zero;
		Sequence s = DOTween.Sequence();

		s.AppendInterval(Random.Range(0f, .2f));
		s.Append(transform.DOScale(localScale, .15f));
	}

	private void OnMouseDown()
	{
		if (!GameManager.Instance.IsPlaying) return;

		Vector3 initScale = transform.localScale;

		Sequence s = DOTween.Sequence();
		s.Append(transform.DOScale(Vector3.zero, .1f));
		s.OnComplete(() =>
		{
			gameObject.SetActive(false);
			transform.localScale = initScale;
		});

		OnClean.Invoke(this);
	}
}
