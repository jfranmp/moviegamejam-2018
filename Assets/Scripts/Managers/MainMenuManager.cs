﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MainMenuManager : MonoBehaviour
{

	public CanvasGroup canvasGroupMainMenu;
	public CanvasGroup canvasGroupInstructions;
	public CanvasGroup canvasGroupFadeOut;

	private bool isChangingScene;
	private bool isExiting;

	private void Awake()
	{
		canvasGroupFadeOut.alpha = 1;

		Sequence s = DOTween.Sequence();
		s.AppendInterval(.5f);
		s.Append(canvasGroupFadeOut.DOAlpha(0f, 1f));
	}

	public void Play()
	{
		if (isChangingScene) return;

		StartCoroutine(ChangeSceneCoroutine());
	}

	public void ShowInstructions()
	{
		canvasGroupMainMenu.DOAlpha(0f, 1f);
		canvasGroupMainMenu.interactable = false;
		canvasGroupMainMenu.blocksRaycasts = false;

		canvasGroupInstructions.DOAlpha(1f, 1f);
		canvasGroupInstructions.interactable = true;
		canvasGroupInstructions.blocksRaycasts = true;
	}

	public void Exit()
	{
		if (isExiting) return;

		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}

	private IEnumerator ChangeSceneCoroutine()
	{
		isChangingScene = true;

		var tween = canvasGroupInstructions.DOAlpha(0f, 1f);
		canvasGroupInstructions.interactable = false;
		canvasGroupInstructions.blocksRaycasts = false;

		var op = SceneManager.LoadSceneAsync("Game");
		op.allowSceneActivation = false;

		yield return new WaitWhile(() => op.progress < .9f || tween.IsPlaying());
		yield return new WaitForSeconds(1f);

		op.allowSceneActivation = true;
	}

}
