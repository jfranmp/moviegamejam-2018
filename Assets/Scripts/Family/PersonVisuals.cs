﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonVisuals : MonoBehaviour
{
	public float movementMultiplier = 100f;
	public float movementSmooth = .2f;
	private Vector3 lastPosition;
	private Vector3 currentSpeed;

	private void OnEnable()
	{
		lastPosition = transform.position;
	}

	private void FixedUpdate()
	{
		currentSpeed = (transform.position - lastPosition) * movementMultiplier;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(-currentSpeed.z, 0, currentSpeed.x), movementSmooth);
		lastPosition = transform.position;
	}
}
