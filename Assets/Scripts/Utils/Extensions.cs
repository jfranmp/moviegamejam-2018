﻿using DG.Tweening;
using UnityEngine;

public static class Extensions
{

	public static T GetComponentInParentRecursively<T>(this GameObject root) where T : Component
	{
		return root.transform.GetComponentInParentRecursively<T>();
	}

	/// <summary>
	/// Return a component looking in the parent
	/// </summary>
	public static T GetComponentInParentRecursively<T>(this Transform root) where T : Component
	{
		T result = root.GetComponentInParent<T>();

		if (result != null) return result;
		else return (root.parent == null) ? null : GetComponentInParentRecursively<T>(root.parent);
	}

	public static Tween DOAlpha(this CanvasGroup canvasGroup, float alpha, float duration)
	{
		return DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, alpha, duration);
	}
}