﻿using DG.Tweening;
using UnityEngine;

public class UIBox : MonoBehaviour
{

	public float delay;
	public float duration = 1f;

	public RectTransform rectTransform;

	private void OnEnable()
	{
		Entry();
	}

	private void Entry()
	{
		Vector2 pos = rectTransform.anchoredPosition;
		rectTransform.anchoredPosition = new Vector2(pos.x, -pos.y);

		Sequence s = DOTween.Sequence();
		s.AppendInterval(delay);
		s.Append(DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, pos, .5f));
	}

}
