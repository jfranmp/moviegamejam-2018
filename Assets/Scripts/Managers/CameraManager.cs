﻿using System.Collections.Generic;
using CF.Utils;
using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{

	public List<GameObject> cameras = new List<GameObject>();
	
	public void SetCamera(int index)
	{
		for (int i = 0; i < cameras.Count; i++)
		{
			cameras[i].SetActive(i == index);
		}
	}
}
