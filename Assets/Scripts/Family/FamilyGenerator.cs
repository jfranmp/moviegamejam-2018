﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class FamilyGenerator : MonoBehaviour
{

	public int minSize = 1;
	public int maxSize = 4;
	[Range(0f, 1f)] public float adultProbability = .6f;

	[Space]
	public float personSize = .25f;
	public float personSeparation = .05f;

	[Header("References")]
	public Person AdultPrefab;
	public Person ChildPrefab;

	[Space][ReadOnly]
	public List<Person> currentFamily = new List<Person>();

	#region [Public API]

	[Button][DisableInEditorMode]
	public void GenerateFamily()
	{
		ClearFamily();

		int personNumber = Random.Range(minSize, maxSize);
		float initOffset = ((personSize * (personNumber - 1)) + (personSeparation * (personNumber - 1))) / 2f;
		for (int i = 0; i < personNumber; i++)
		{
			Person current = Instantiate(Random.Range(0f, 1f) < adultProbability ? AdultPrefab : ChildPrefab, transform.position - (initOffset - (personSize + personSeparation) * i) * Vector3.right, Quaternion.identity);
			current.transform.SetParent(transform, true);

			currentFamily.Add(current);
		}

		gameObject.AddComponent<Family>();
		Destroy(this);

		gameObject.name = "Family";
	}

	#endregion

	#region Helpers

	private void ClearFamily()
	{
		while (currentFamily.Count > 0)
		{
			Destroy(currentFamily[0].gameObject);
			currentFamily.RemoveAt(0);
		}
	}

	#endregion

}
