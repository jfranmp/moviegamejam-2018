﻿public static class GameRules
{
	public const int CLEAN_SCORE = 1;
	public const int ADULT_HAPPY_SCORE = 5;
	public const int ADULT_SAD_SCORE = -3;
	public const int CHILD_HAPPY_SCORE = 5;
	public const int CHILD_SAD_SCORE = -2;
	public const int DROP_PERSON_SCORE = 5;
}
