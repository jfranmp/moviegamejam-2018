﻿using CF.Utils.Events;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class Person : MonoBehaviour
{

	public static EventX<Person> OnPlacePerson = new EventX<Person>();

	public PersonType personType = PersonType.Adult;
	public GameObject happyEmote;
	public GameObject sadEmote;

	[ReadOnly]
	public Chair chair;

	#region Properties

	public bool IsSit
	{
		get { return chair != null; }
	}

	#endregion

	#region [Public API]

	public void Place(Chair chair)
	{
		this.chair = chair;
		chair.personInside = this;

		// Move to chair
		transform.SetParent(chair.transform, true);

		Sequence s = DOTween.Sequence();
		s.AppendInterval(Random.Range(0f, .1f));
		s.Append(transform.DOLocalMove(transform.localPosition / 2 + Vector3.up, Random.Range(.1f, .125f)).SetEase(Ease.InSine));
		s.Append(transform.DOLocalMove(Vector3.zero, Random.Range(.1f, .125f)).SetEase(Ease.OutSine));

		OnPlacePerson.Invoke(this);
	}

	public int Score()
	{
		if (chair == null) return 0;
		return IsHappy() ? 
			(personType == PersonType.Adult ? GameRules.ADULT_HAPPY_SCORE : GameRules.CHILD_HAPPY_SCORE) : 
			(personType == PersonType.Adult ? GameRules.ADULT_SAD_SCORE : GameRules.CHILD_SAD_SCORE);
	}

	public void CheckDirt()
	{
		if (chair != null)
		{
			chair.CheckDirt((personType == PersonType.Adult ? .075f : .15f) * (IsHappy() ? 1 : 2));
		}
	}

	public void ShowEmote()
	{
		if (IsHappy()) ShowHappy();
		else ShowSad();
	}

	#endregion

	#region Helpers

	private bool IsHappy()
	{
		switch (personType)
		{
			case PersonType.Adult: return ChildAround() <= 2;
			case PersonType.Child: return !chair.AdultInSouth();
		}

		return true;
	}

	private int ChildAround()
	{
		if (chair == null) return 0;

		int count = 0;

		if (chair.North != null && chair.North.personInside != null && chair.North.personInside.personType == PersonType.Child) count++;
		if (chair.South != null && chair.South.personInside != null && chair.South.personInside.personType == PersonType.Child) count++;
		if (chair.East != null && chair.East.personInside != null && chair.East.personInside.personType == PersonType.Child) count++;
		if (chair.West != null && chair.West.personInside != null && chair.West.personInside.personType == PersonType.Child) count++;

		return count;
	}

	private void ShowHappy()
	{
		happyEmote.transform.localScale = Vector3.zero;
		happyEmote.SetActive(true);

		Sequence s = DOTween.Sequence();
		s.Append(happyEmote.transform.DOScale(Vector3.one * .3f, .2f));
		s.AppendInterval(2f);
		s.Append(happyEmote.transform.DOScale(Vector3.zero, .1f));
		s.OnComplete(() =>
		{
			happyEmote.SetActive(false);
		});
	}

	private void ShowSad()
	{
		sadEmote.transform.localScale = Vector3.zero;
		sadEmote.SetActive(true);

		Sequence s = DOTween.Sequence();
		s.Append(sadEmote.transform.DOScale(Vector3.one * .3f, .2f));
		s.AppendInterval(2f);
		s.Append(sadEmote.transform.DOScale(Vector3.zero, .1f));
		s.OnComplete(() =>
		{
			sadEmote.SetActive(false);
		});
	}

	#endregion

	#region Nested

	public enum PersonType
	{
		Adult, Child
	}

	#endregion

}
