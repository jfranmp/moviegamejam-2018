﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightNoise : MonoBehaviour
{

	public float intensity = 1f;
	public float freq;
	public Gradient colorRandom;

	private float offset;
	private new Light light;

	private void Awake()
	{
		light = GetComponent<Light>();
		offset = Random.Range(0f, 1f);
	}

	private void Update()
	{
		light.intensity = intensity;
		light.color = colorRandom.Evaluate(Mathf.PerlinNoise(Time.time * freq, offset));
	}

}
