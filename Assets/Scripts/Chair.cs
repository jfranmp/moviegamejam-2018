﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class Chair : MonoBehaviour
{

	public static List<Chair> allChairs = new List<Chair>();
	public static Dictionary<Vector3, Chair> chairsDatabase = new Dictionary<Vector3, Chair>();

	public bool doEntry = true;
	public Person personInside;
	public List<Dirt> dirtList;
	public bool broken;

	[Header("References")]
	public GameObject brokenGameObject;

	private bool _broken;

	#region Properties

	public bool CanSit { get { return IsFree && !IsDirty && !broken; } }
	public bool IsFree { get { return personInside == null; } }
	public bool IsDirty { get { return dirtList.Find(x => x.gameObject.activeInHierarchy) != null; } }

	[ShowInInspector]
	[ReadOnly]
	public Chair North { get; private set; }

	[ShowInInspector]
	[ReadOnly]
	public Chair South { get; private set; }

	[ShowInInspector]
	[ReadOnly]
	public Chair East { get; private set; }

	[ShowInInspector]
	[ReadOnly]
	public Chair West { get; private set; }

	public Vector3 ChairPosition
	{
		get { return new Vector3(transform.position.x, 0, transform.position.z); }
	}

	#endregion

	#region Unity Events

	private void OnEnable()
	{
		if (doEntry)
		{
			Entry();
		}

		Register(this);
		Init(this);
	}

	private void OnDisable()
	{
		Unregister(this);
	}

	private void Update()
	{
		if (_broken != broken)
		{
			brokenGameObject.SetActive(broken);
			_broken = broken;
		}
	}

	#endregion

	#region [Public API]

	public bool AdultInSouth()
	{
		if (South == null) return false;
		else
		{
			if (South.personInside != null && South.personInside.personType == Person.PersonType.Adult) return true;
			else
			{
				return South.AdultInSouth();
			}
		}
	}

	public bool CanHoldEast(int count)
	{
		if (!CanSit) return false;
		if (count <= 1) return true;
		return East == null ? false : East.CanHoldEast(count - 1);
	}

	public bool CanHoldWest(int count)
	{
		if (!IsFree) return false;
		if (count <= 1) return true;
		return West == null ? false : West.CanHoldWest(count - 1);
	}

	public void CheckDirt(float probability)
	{
		dirtList.ForEach(dirt =>
		{
			if (Random.Range(0f, 1f) < probability) dirt.gameObject.SetActive(true);
		});
	}

	#endregion

	#region Tween

	public void Entry()
	{
		Vector3 endScale = transform.localScale;
		transform.localScale = Vector3.zero;
		transform.position += Vector3.up;

		Sequence s = DOTween.Sequence();
		s.AppendInterval(Random.Range(0f, .1f));
		s.Append(transform.DOScale(endScale, 1f).SetEase(Ease.OutBack));
		s.Join(transform.DOMove(transform.position - Vector3.up, 1f).SetEase(Ease.OutBounce));
	}

	#endregion

	#region Editor

	[Button][HideInPlayMode]
	private void SetupDirt()
	{
		dirtList = GetComponentsInChildren<Dirt>(true).ToList();
	}

	#endregion

	#region Static

	private static void Init(Chair chair)
	{
		Vector3 northPos = chair.ChairPosition + Vector3.forward;
		Vector3 southPos = chair.ChairPosition - Vector3.forward;
		Vector3 eastPos = chair.ChairPosition + Vector3.right;
		Vector3 westPos = chair.ChairPosition - Vector3.right;

		// Two way bindings

		if (chairsDatabase.ContainsKey(northPos))
		{
			chair.North = chairsDatabase[northPos];
			chairsDatabase[northPos].South = chair;
		}

		if (chairsDatabase.ContainsKey(southPos))
		{
			chair.South = chairsDatabase[southPos];
			chairsDatabase[southPos].North = chair;
		}

		if (chairsDatabase.ContainsKey(eastPos))
		{
			chair.East = chairsDatabase[eastPos];
			chairsDatabase[eastPos].West = chair;
		}

		if (chairsDatabase.ContainsKey(westPos))
		{
			chair.West = chairsDatabase[westPos];
			chairsDatabase[westPos].East = chair;
		}
	}

	private static void Register(Chair chair)
	{
		allChairs.Add(chair);
		if (!chairsDatabase.ContainsKey(chair.ChairPosition))
			chairsDatabase.Add(chair.ChairPosition, null);

		chairsDatabase[chair.ChairPosition] = chair;
	}

	private static void Unregister(Chair chair)
	{
		allChairs.Remove(chair);
		chairsDatabase.Remove(chair.ChairPosition);
	}

	#endregion

}
