﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
	public List<Door> enterDoors = new List<Door>();
	public List<Door> exitDoors = new List<Door>();

	private void OnEnable()
	{
		InputManager.OnSelectedObject += OnSelectedObject;
	}

	private void OnDisable()
	{
		InputManager.OnSelectedObject -= OnSelectedObject;
	}

	private void OnSelectedObject(GameObject selected)
	{
		if (selected == null) SetExit(false);
		else
		{
			SetExit(selected.GetComponent<Family>() != null);
		}
	}

	public void SetEnter(bool state)
	{
		if (state) enterDoors.ForEach(x => x.Open());
		else enterDoors.ForEach(x => x.Close());
	}

	public void SetExit(bool state)
	{
		if (state) exitDoors.ForEach(x => x.Open());
		else exitDoors.ForEach(x => x.Close());
	}
}
