﻿using UnityEngine;

namespace CF.Utils
{

    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T m_instance;
        public static T Instance
        {
            get
            {
                if (m_instance == null) m_instance = FindObjectOfType<T>();
                if (m_instance == null) Debug.LogError("Don't exist an instance of " + typeof(T));

                return m_instance;
            }
        }
    }

}