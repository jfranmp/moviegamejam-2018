﻿using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class LightManager : MonoBehaviour
{
	public List<Light> baseLights = new List<Light>();
	public List<LightNoise> screenLights = new List<LightNoise>();

	[Header("Base lights")]
	public float baseLightIntensityMax = 1f;
	public float baseLightIntensityMin = .25f;

	[Header("Screen lights")]
	public float screenLightsMax = 1.5f;
	public float screenLightsMin = 0f;

	[Button]
	public void SetCinemaLights()
	{
		Sequence s = DOTween.Sequence();
		s.Append(SetScreenLights(false));
		s.Join(SetBaseLights(true));
	}

	[Button]
	public void SetFilmLights()
	{
		Sequence s = DOTween.Sequence();
		s.Append(SetBaseLights(false));
		s.Append(SetScreenLights(true));
	}

	public Tween SetBaseLights(bool state)
	{
		float targetIntensity = state ? baseLightIntensityMax : baseLightIntensityMin;

		Sequence s = DOTween.Sequence();
		baseLights.ForEach(x =>
		{
			s.Join(x.DOIntensity(targetIntensity, 1f));
		});

		return s;
	}

	public Tween SetScreenLights(bool state)
	{
		float targetIntensity = state ? screenLightsMax : screenLightsMin;

		Sequence s = DOTween.Sequence();
		screenLights.ForEach(x =>
		{
			s.Join(DOTween.To(() => x.intensity, y => x.intensity = y, targetIntensity, 2f));
		});

		return s;
	}
}
