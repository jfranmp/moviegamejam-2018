﻿using UnityEngine;

public class Hoverable : MonoBehaviour
{
	protected bool hovered;

	protected virtual void OnEnable()
	{
		InputManager.OnChangeHoverObject += OnChangeHoverObject;
	}

	protected virtual void OnDisable()
	{
		InputManager.OnChangeHoverObject -= OnChangeHoverObject;
	}

	protected virtual void OnChangeHoverObject(GameObject hoverObject)
	{
		if (!hovered && hoverObject == gameObject) SetHover();
		else if (hovered && hoverObject != gameObject) QuitHover();
	}

	protected virtual void SetHover()
	{

	}

	protected virtual void QuitHover()
	{

	}
}
