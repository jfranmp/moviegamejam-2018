﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using DG.Tweening;

public class Door : MonoBehaviour
{

	public Vector3 openRotation;
	public Vector3 closeRotation;

	[Button]
	public void Open()
	{
		transform.DORotate(openRotation, .5f);
	}

	[Button]
	public void Close()
	{
		transform.DORotate(closeRotation, .5f);
	}
}
