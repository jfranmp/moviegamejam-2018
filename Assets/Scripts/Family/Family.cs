﻿using System.Collections.Generic;
using System.Linq;
using CF.Utils.Events;
using DG.Tweening;
using UnityEngine;

public class Family : Selectable
{

	public static List<Family> allFamilies = new List<Family>();
	public static EventX<Family> OnPlaceFamily = new EventX<Family>();
	public static EventX<Family> OnDropFamily = new EventX<Family>();

	public List<Person> personList = new List<Person>();

	#region Unity Events

	private void Awake()
	{
		personList = GetComponentsInChildren<Person>().ToList();
	}

	protected override void OnEnable()
	{
		base.OnEnable();

		allFamilies.Add(this);
	}

	protected override void OnDisable()
	{
		base.OnDisable();

		allFamilies.Remove(this);
	}

	#endregion

	#region [Public API]

	public bool CanPlaceFamily(Chair chair)
	{
		return chair.CanHoldEast(personList.Count);
	}

	public void PlaceFamily(Chair chair)
	{
		if (CanPlaceFamily(chair))
		{
			var currentChair = chair;
			for (int i = 0; i < personList.Count; i++)
			{
				personList[i].Place(currentChair);
				currentChair = currentChair.East;
			}

			OnPlaceFamily.Invoke(this);

			Destroy(gameObject);
		}
	}

	public void Drop()
	{
		InputManager.OnSelectedObject -= OnSelectedObject;

		OnDropFamily.Invoke(this);

		Sequence s = DOTween.Sequence();
		s.Append(transform.DOMove(GameManager.Instance.exitPosition.position - Vector3.forward, .15f));
		s.Append(transform.DOMove(GameManager.Instance.exitPosition.position + Vector3.forward * 3, .25f));
		s.OnComplete(() =>
		{
			Destroy(gameObject);
		});
	}

	#endregion

	#region Selectable

	protected override void SetSelected()
	{
		transform.DOMove(transform.position + Vector3.up * .35f, .25f);
	}

	protected override void QuitSelected()
	{
		transform.DOMove(transform.position - Vector3.up * .35f, .25f);
	}

	#endregion

}
